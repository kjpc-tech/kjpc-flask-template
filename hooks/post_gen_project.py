import os


if "{{ cookiecutter.use_db }}".lower() == "n":
    os.remove("{{ cookiecutter.project_name }}/database.py")
    os.remove("{{ cookiecutter.project_name }}/models.py")


if "{{ cookiecutter.use_vuejs }}".lower() == "n":
    os.remove("webpack/src/vueapp.js")
