import os

from flask import Flask, render_template

import {{ cookiecutter.project_name }}.logging

from {{ cookiecutter.project_name }} import settings
{% if cookiecutter.use_db == "y" %}from {{ cookiecutter.project_name }}.database import db{% endif %}


app = Flask(__name__)
{% if cookiecutter.use_db == "y" %}
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{settings.DATABASE_PATH}'

#initialize the database
db.init_app(app)


@app.cli.command('initdb')
def initdb_command():
    db.create_all()
{% endif %}

@app.errorhandler(400)
def bad_request(error):
    return render_template('400.html'), 400


@app.errorhandler(403)
def permission_denied(error):
    return render_template('403.html'), 403


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(error):
    return render_template('500.html'), 500


@app.context_processor
def base_context():
    # directory where bundles can be found
    bundles_dir = settings.STATIC_BUNDLE_DIR
    bundles_dir_relative = bundles_dir.split('/static/', 1)[1]

    # bundles to include in the site, should be ordered
    bundles_names = settings.STATIC_BUNDLE_NAMES

    bundles_css = []
    bundles_js = []

    # go through bundle directory and find latest bundles
    for bundle_index, bundle_name in enumerate(bundles_names):
        bundles_css.append(None)
        bundles_js.append(None)
        for file_name in os.listdir(bundles_dir):
            if file_name.startswith(bundle_name):
                modification_time = os.path.getmtime(os.path.join(bundles_dir, file_name))
                if file_name.endswith('.css') and (bundles_css[bundle_index] is None or modification_time >= bundles_css[bundle_index][1]):
                    bundles_css[bundle_index] = (os.path.join(bundles_dir_relative, file_name), modification_time)
                if file_name.endswith('.js') and (bundles_js[bundle_index] is None or modification_time >= bundles_js[bundle_index][1]):
                    bundles_js[bundle_index] = (os.path.join(bundles_dir_relative, file_name), modification_time)

    return {
        'is_debug': settings.DEBUG,
        'bundles_css': bundles_css,
        'bundles_js': bundles_js,
    }


@app.route("/")
def index_view():
    return render_template('index.html'), 200


@app.route("/page-1/")
def page1_view():
    return render_template('page1.html'), 200


@app.route("/page-2/")
def page2_view():
    return render_template('page2.html'), 200
