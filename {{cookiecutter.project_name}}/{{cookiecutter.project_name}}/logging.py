import logging
import os

from logging.config import dictConfig

from {{ cookiecutter.project_name }} import settings


class InfoOnlyFilter(logging.Filter):
    def filter(self, record):
        return record.levelno == logging.INFO


LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'file_simple': {
            'format': '%(levelname)s %(asctime)s %(message)s',
        }
    },
    'filters': {
        'info_only': {
            '()': InfoOnlyFilter,
        },
    },
    'handlers': {
        'flask_file_error': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(settings.ROOT_DIR, 'logs/flask_error_log.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
        },
        'flask_file_info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(settings.ROOT_DIR, 'logs/flask_info_log.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
            'filters': ['info_only'],
        },
        'file_default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(settings.ROOT_DIR, 'logs/default.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
        },
    },
    'loggers': {
        'flask.app': {
            'handlers': ['flask_file_error', 'flask_file_info'],
            'level': 'INFO',
            'propagate': True,
        },
        '{{ cookiecutter.project_name }}.default': {
            'handlers': ['file_default'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

# add emailing admins logging handler
if getattr(settings, 'EMAIL_HOST', None) and getattr(settings, 'DEFAULT_FROM_EMAIL', None):
    LOGGING_CONFIG['handlers']['mail_admins'] = {
        'level': 'ERROR',
        'class': 'logging.handlers.SMTPHandler',
        'mailhost': (settings.EMAIL_HOST, getattr(settings, 'EMAIL_PORT', 25)),
        'fromaddr': settings.DEFAULT_FROM_EMAIL,
        'toaddrs': getattr(settings, 'ADMINS', []),
        'subject': '{{ cookiecutter.project_name }} ERROR',
    }

    LOGGING_CONFIG['loggers']['flask.app']['handlers'].append('mail_admins')


# enable logging configuration
dictConfig(LOGGING_CONFIG)
