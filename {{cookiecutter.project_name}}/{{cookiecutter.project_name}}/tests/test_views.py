def test_index_view(client):
    response = client.get('/')

    assert response.status_code == 200

    assert 'Index Page' in str(response.get_data())


def test_page1_view(client):
    response = client.get('/page-1/')

    assert response.status_code == 200

    assert 'Page 1' in str(response.get_data())


def test_page2_view(client):
    response = client.get('/page-2/')

    assert response.status_code == 200

    assert 'Page 2' in str(response.get_data())
