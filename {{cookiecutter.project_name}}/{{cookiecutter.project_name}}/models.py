from {{ cookiecutter.project_name }}.database import db


# An example model
# class MyModel(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(100), nullable=False)
#     number = db.Column(db.Integer, nullable=True)

#     def __str__(self):
#         return self.name
