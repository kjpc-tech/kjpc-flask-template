import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.dirname(BASE_DIR)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', '{% if cookiecutter.debug %}True{% else %}False{% endif %}') == 'True'

{% if cookiecutter.use_db == "y" %}DATABASE_PATH = os.path.join(ROOT_DIR, '{{ cookiecutter.project_name }}.sqlite3'){% endif %}

STATIC_BUNDLE_DIR = os.path.join(BASE_DIR, 'static', 'bundles')
STATIC_BUNDLE_NAMES = [
    'vendors~index',{% if cookiecutter.use_vuejs == "y" %}
    'vendors~vueapp',{% endif %}
    'index',{% if cookiecutter.use_vuejs == "y" %}
    'vueapp',{% endif %}
    'mainapp',
]
