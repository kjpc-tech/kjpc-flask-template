import os
import re

from invoke import task, Collection


current_dir = os.path.dirname(os.path.abspath(__file__))
site_dir = os.path.join(current_dir, "{{ cookiecutter.project_name }}")
venv = os.path.join(current_dir, "venv")
ansible_dir = os.path.join(current_dir, "ansible")

# make sure this is the same as `main_user_name` in `ansible/vars/secrets.yml`
ansible_default_user = 'webber'


def _run_command(ctx, command, directory=current_dir):
    print(f"Running '{command}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(directory):
            ctx.run(command, pty=True)


def _get_flask_environment():
    from {{ cookiecutter.project_name }}.settings import DEBUG

    if DEBUG:
        return 'development'
    else:
        return 'production'


@task
def test(ctx):
    """
    Run the test command.
    """

    cmd = 'pytest'
    _run_command(ctx, cmd, directory=site_dir)

{% if cookiecutter.use_db == "y" %}
@task
def initdb(ctx):
    """
    Run the flask initdb command. Only works if a database is configured.
    """

    cmd = f'FLASK_ENV={_get_flask_environment()} flask initdb'
    _run_command(ctx, cmd, directory=site_dir)
{% endif %}

@task
def runserver(ctx):
    """
    Run the flask run command.
    """

    cmd = f'FLASK_ENV={_get_flask_environment()} flask run --port 8000'
    _run_command(ctx, cmd, directory=site_dir)


@task
def shell(ctx):
    """
    Run the flask shell command.
    """

    cmd = f'FLASK_ENV={_get_flask_environment()} flask shell'
    _run_command(ctx, cmd, directory=site_dir)


@task
def runwebpack(ctx, debug=True, watch=True):
    """
    Run the webpack command.
    """
    cmd = 'npm run build'
    if debug:
        cmd += '-dev'
    if watch:
        cmd += '-watch'
    _run_command(ctx, cmd)


@task
def setup(ctx):
    """
    Setup the development environment.
    """
    # create virtual environment if it doesn't exist
    if not os.path.exists(os.path.join(venv, "bin", "activate")):
        print(f"Creating virtual environment '{venv}'.")
        ctx.run(f'python3 -m venv {venv}', pty=True)
    else:
        print(f"Virtual environment '{venv}' not created because it already exists.")

    # install dependencies
    with ctx.prefix(f'source {venv}/bin/activate'):
        with open(os.path.join(current_dir, "requirements", "pinned.txt")) as f:
            has_pinned_dependencies = 'flask' in f.read()
        if has_pinned_dependencies:
            # install pinned dependencies
            print("Installing dependencies from requirements/pinned.txt.")
            ctx.run('pip install -r requirements/pinned.txt', pty=True)
        else:
            # install normal dependencies
            print("Installing dependencies from requirements/base.txt.")
            ctx.run('pip install -r requirements/base.txt', pty=True)

            # freeze dependencies
            print("Freezing dependencies into requirements/pinned.txt.")
            ctx.run('pip freeze | grep -v "pkg-resources" > requirements/pinned.txt', pty=True)

    # install node modules
    with ctx.prefix(f'source {venv}/bin/activate'):
        print("Creating node environment.")
        ctx.run('nodeenv --python-virtualenv', pty=True)
        print("Installing node package.")
        if os.path.exists(os.path.join(current_dir, "package-lock.json")):
            ctx.run('npm ci', pty=True)
        else:
            ctx.run('npm install', pty=True)
        print("Building initial assets.")
        runwebpack(ctx, debug=True, watch=False)

    {% if cookiecutter.use_db == "y" %}
    # initialize database
    initdb(ctx)
    {% endif %}

    # run tests
    test(ctx)


@task
def deploy_setup(ctx):
    """
    First time setup for deployment. Add required submodules.
    """
    ansible_relative_dir = ansible_dir.replace(current_dir, '.')
    for repo_url, repo_path in [
        ("https://gitlab.com/kjpc-tech/kjpc-ansible-roles.git", os.path.join(ansible_relative_dir, "vendor", "kjpc-ansible-roles")),
        ("https://gitlab.com/cdetar/cfd-common-roles.git", os.path.join(ansible_relative_dir, "vendor", "cfd-common-roles")),
    ]:
        cmd = f'git submodule add {repo_url} {repo_path}'
        ctx.run(cmd, pty=True)


@task
def deploy(ctx, user=ansible_default_user, tags=None):
    """
    Deploy {{ cookiecutter.project_name }}.
    """
    tags = f'--tags {tags}' if tags is not None else ''
    cmd = f'ansible-playbook --inventory hosts.yml deploy-prod.yml --user {user} --ask-vault-pass {tags}'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_update(ctx, user=ansible_default_user):
    """
    Update the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml updates.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_reboot(ctx, user=ansible_default_user):
    """
    Reboot the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml reboot.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


site = Collection('site')
site.add_task(setup)
site.add_task(test)
{% if cookiecutter.use_db == "y" %}site.add_task(initdb){% endif %}
site.add_task(runserver)
site.add_task(shell)
site.add_task(runwebpack)

remote = Collection('remote')
remote.add_task(remote_update, 'update')
remote.add_task(remote_reboot, 'reboot')
remote.add_task(deploy_setup, 'setup')
remote.add_task(deploy)

namespace = Collection()
namespace.add_collection(site)
namespace.add_collection(remote)
