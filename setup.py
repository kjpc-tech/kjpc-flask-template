import setuptools


setuptools.setup(
    name="kjpc-flask-template",
    author="Kyle Johnson",
    author_email="kyle@kjpc.tech",
    license="MIT",
    url="https://gitlab.com/kjpc-tech/kjpc-flask-template/",
    description="Flask website template,",
    python_requires=">=3.6",
    install_requires=[
        "cookiecutter",
        "pytest",
        "pytest-cookies",
    ],
)
