# kjpc-flask-template

[![pipeline status](https://gitlab.com/kjpc-tech/kjpc-flask-template/badges/master/pipeline.svg)](https://gitlab.com/kjpc-tech/kjpc-flask-template/commits/master)

## Basic [Flask](http://flask.pocoo.org/) web application template.
Flask web application template using [Flask](http://flask.pocoo.org/), [Bootstrap4](https://getbootstrap.com/docs/4.1/getting-started/introduction/), and [webpack](https://webpack.js.org/). Optionally includes a database and [Vue.js](https://vuejs.org/).

Designed for and works well with [kjpc-ansible-roles](https://gitlab.com/kjpc-tech/kjpc-ansible-roles).

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

## Installation (requires [Python3](https://www.python.org/))
1. Install [cookiecutter](https://github.com/cookiecutter/cookiecutter) if not installed
    - `pip3 install cookiecutter`
2. Create project from template
    - `cookiecutter https://gitlab.com/kjpc-tech/kjpc-flask-template`
3. Change directories to project root
    - `cd /path/to/new/project`
4. Install [invoke](http://www.pyinvoke.org/index.html) if not installed
    - `pip3 install invoke`
5. Setup local environment
    - `invoke site.setup`

---

## Development (requires [Python3](https://www.python.org/))
1. Run development server
    - `invoke site.runserver`
2. Run webpack
    - `invoke site.runwebpack`

---

## Deployment (requires [Python3](https://www.python.org/), [Git](https://git-scm.com/), [Ansible](https://www.ansible.com/))
1. Make sure the git repository is initialized and hosted somewhere
2. Do initial setup
    - `invoke remote.setup`
    - This will add required submodules
3. Edit `ansible/hosts.yml`
    - Change the example host to point to a remote VPS (such a DigitalOcean Droplet or AWS EC2)
4. Update variables
    - Edit `ansible/vars/vars.yml`
    - Create and edit `ansible/vars/secrets.yml` with `ansible-vault`
5. Update deployment play
    - Update `ansible/deploy-prod.yml` to fit specific needs
    - (nginx config, ssl, monit)
6. First Deploy
    - `invoke remote.deploy --user=root --tags=apt,postfix,users` (deploy project basics using `root` user)
    - This will only work the first time because the `root` user login will be disabled. Use the newly created user afterwards
    - Make sure `main_user_name` in `ansible/vars/secrets.yml` matches `ansible_default_user` in `tasks.py` for convenience
7. Deploy
    - `invoke remote.deploy` (deploy entire project from bottom to top)
    - `invoke remote.deploy --tags=flask` (deploy flask portion of project)
    - `invoke remote.deploy --tags=monit` (deploy monit portion of project)
8. Updates and Rebooting
    - `invoke remote.update`
    - `invoke remote.reboot`

---

## CI/CD (requires [Git](https://git-scm.com/), [GitLab](https://gitlab.com/))
1. Make sure the git repository is initialized and hosted on [GitLab](https://gitlab.com/)
2. Set `SSH_PRIVATE_KEY_VAL` in GitLab CI/CD settings to the SSH private key that can access the remote host
3. Set `ANSIBLE_VAULT_PASS_VAL` in GitLab CI/CD settings to the Ansible Vault password for the encrypted variables
4. Push changes to GitLab
5. GitLab will test and deploy the project
6. More Information at [https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)

---

### Screenshot
![](screenshot.png)

### Change Log
#### 1.3
  - Switch to cookiecutter template
  - Switch to `npm ci` in `.gitlab-ci.yml`

#### 1.2
  - Switched to requirements folder

#### 1.1
  - Added ansible deployment configuration
  - Added basic CI configuration
  - Added tests and CI template
  - Added support for an sqlite database
  - Changed default deployment `flask_node_commands`
  - Fixed submodule initialization
  - Added deployment configuration for database
  - Added testing to CI template
  - Added logging configuration
  - Added support for k-flask settings (kjpc-ansible-roles)

#### 1.0
  - Initial version

### License
[MIT License](LICENSE)
